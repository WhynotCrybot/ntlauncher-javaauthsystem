package ru.NTSystem.NTConnector.Databases;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.bukkit.configuration.file.YamlConfiguration;

public class YMLStorage extends Storage {
	private YamlConfiguration storage;
	private File stfile;
	private List<String> users = new ArrayList<String>();

	public YMLStorage(File stfile) {
		this.stfile = stfile;
		if (!stfile.exists())
			try {
				stfile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		storage = YamlConfiguration.loadConfiguration(stfile);
		if (null != storage.getConfigurationSection("Users"))
			for (String user : storage.getConfigurationSection("Users")
					.getKeys(false)) {
				users.add(user);
			}
	}

	@Override
	public void registerUser(String name, String password, String email) {
		storage.set("Users." + name + ".password", password);
		storage.set("Users." + name + ".mail", email);
		users.add(name);
		save();
	}

	@Override
	public boolean authorizeUser(String name, String password) {
		return storage.isSet("Users." + name + ".password")
				&& storage.getString("Users." + name + ".password").equals(
						password);
	}

	@Override
	public void banHWID(String hwid) {
		List<String> hwids = getBannedHWIDs();
		if (!hwids.contains(hwid)) {
			hwids.add(hwid);
			storage.set("Banned-hwids", hwids);
			save();
		}
	}

	@Override
	public void banUser(String name) {
		for (String hwid : getUserHWIDs(name)) {
			banHWID(hwid);
		}
	}

	@Override
	public List<String> getUserHWIDs(String name) {
		return storage.getStringList("User-hwids." + name);
	}

	@Override
	public boolean isRegistered(String name, String email) {
		if (storage.isSet("Users." + name + ".mail"))
			return true;
		for (String user : users) {
			if (storage.getString("Users." + user + ".mail").equals(email))
				return true;
		}
		return false;
	}

	@Override
	public boolean isHWIDBanned(String hwid) {
		return getBannedHWIDs().contains(hwid);
	}

	@Override
	public void addUserHWID(String user, String hwid) {
		List<String> hwids = getUserHWIDs(user);
		if (!hwids.contains(hwid)) {
			hwids.add(hwid);
			storage.set("User-hwids." + user, hwids);
			save();
		}
	}

	@Override
	public String getType() {
		return "FlatFile";
	}

	public List<String> getBannedHWIDs() {
		return storage.getStringList("Banned-hwids");
	}

	public void save() {
		try {
			storage.save(stfile);
		} catch (Exception e) {
			Logger.getLogger("Minecraft")
					.warning("File " + stfile.getAbsolutePath() + " is wrong");
		}
	}

}
