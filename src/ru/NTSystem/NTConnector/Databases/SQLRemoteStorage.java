package ru.NTSystem.NTConnector.Databases;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SQLRemoteStorage extends SQLStorage {

    protected String host, user, pass, db;
    protected int port;

    public SQLRemoteStorage(String host, int port, String user, String pass, String db){
        this.host = host;
        this.port = port;
        this.user = user;
        this.pass = pass;
        this.db = db;
    }

    @Override
    public void registerUser(String name, String password, String email) {
        sql(String.format("INSERT INTO `%s` (`login`, `password`, `mail`) VALUES ('%s', '%s', '%s')",
                usersTable, name, password, email));
    }

    @Override
    public boolean authorizeUser(String name, String password) {
        String query = String.format("SELECT COUNT(*) FROM `%s` WHERE `login`='%s' AND `password`='%s'", usersTable, name, password);
        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://"
                    + host + ":" + port + "/" + db + "?autoReconnect=true&user=" + user
                    + "&password=" + pass);
            ResultSet rs = conn.createStatement().executeQuery(query);
            rs.next();
            if (rs.getInt(1) != 0) {
                rs.close();
                conn.close();
                return true;
            }
            return false;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void banHWID(String hwid) {
        sql(String.format("INSERT INTO `%s` (`hwid`) VALUES ('%s')", banHWIDsTable, hwid));
    }

    @Override
    public void banUser(String name) {
        for (String hwid : getUserHWIDs(name)) {
            banHWID(hwid);
        }
    }

    @Override
    public void addUserHWID(String user, String hwid) {
        sql(String.format("INSERT INTO `%s` (`login`, `hwid`) VALUES ('%s', '%s')", usersHWIDsTable, user, hwid));
    }

    @Override
    public List<String> getUserHWIDs(String name) {
        List<String> hwids = new ArrayList<String>();
        String query = String.format("SELECT `hwid` FROM `%s` WHERE `login`='%s'", usersHWIDsTable, name);
        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://"
                    + host + ":" + port + "/" + db + "?autoReconnect=true&user=" + user
                    + "&password=" + pass);
            ResultSet rs = conn.createStatement().executeQuery(query);
            while (rs.next()) {
                hwids.add(rs.getString("hwid"));
            }
            rs.close();
            conn.close();
            return hwids;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return hwids;
    }

    @Override
    public boolean isRegistered(String name, String email) {
        String query = String.format("SELECT COUNT(*) FROM `%s` WHERE `login`='%s' AND `mail`='%s'", usersTable, name, email);
        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://"
                    + host + ":" + port + "/" + db + "?autoReconnect=true&user=" + user
                    + "&password=" + pass);
            PreparedStatement ps = conn.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                if (rs.getInt(1) != 0) {
                    ps.close();
                    conn.close();
                    return true;
                }
            }
            ps.close();
            conn.close();
            return false;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean isHWIDBanned(String hwid) {
        String query = String.format("SELECT COUNT(*) FROM `%s` WHERE `hwid`='%s'", banHWIDsTable, hwid);
        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://"
                    + host + ":" + port + "/" + db + "?autoReconnect=true&user=" + user
                    + "&password=" + pass);
            PreparedStatement ps = conn.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                if (rs.getInt(1) != 0) {
                    ps.close();
                    return true;
                }
            }
            ps.close();
            conn.close();
            return false;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    protected void sql(String sql){
        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://"
                    + host + ":" + port + "/" + db + "?autoReconnect=true&user=" + user
                    + "&password=" + pass);
            conn.createStatement().execute(sql);
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
