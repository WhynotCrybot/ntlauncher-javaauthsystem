package ru.NTSystem.NTConnector.Databases;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MySQLStorage extends SQLRemoteStorage {
	public static final String sqlUserTable = String.format("CREATE TABLE IF NOT EXISTS `%s` ("
			+ "`id` INT(5) NOT NULL AUTO_INCREMENT,"
			+ "`login` varchar(150) NOT NULL,"
			+ "`password` varchar(96) NOT NULL,"
			+ "`mail` varchar(150) NOT NULL,"
			+ " UNIQUE KEY `login` (`login`,`mail`), PRIMARY KEY (`id`)"
			+ ") ENGINE=InnoDB DEFAULT CHARSET=latin1;", SQLStorage.usersTable);
	public static final String sqlBannedHWIDsTable = String.format("CREATE TABLE IF NOT EXISTS `%s` ("
			+ "`id` INT(5) NOT NULL AUTO_INCREMENT,"
			+ "`hwid` varchar(150) NOT NULL,"
			+ " PRIMARY KEY (`id`)"
			+ ") ENGINE=InnoDB DEFAULT CHARSET=latin1;", SQLStorage.banHWIDsTable);
	public static final String sqlUserHWIDsTable = String.format("CREATE TABLE IF NOT EXISTS `%s` ("
			+ "`id` INT(5) NOT NULL AUTO_INCREMENT,"
			+ "`login` varchar(150) NOT NULL,"
			+ "`hwid` varchar(96) NOT NULL,"
			+ " PRIMARY KEY (`id`)" + ") ENGINE=InnoDB DEFAULT CHARSET=latin1;", SQLStorage.usersHWIDsTable);

	public MySQLStorage(String host, int port, String user, String pass,
			String db) throws SQLException, ClassNotFoundException {
		super(host, port, user, pass, db);

		Class.forName("com.mysql.jdbc.Driver");
		createTables();
	}

	public void createTables() {
		try {
			sql(sqlUserTable);
			sql(sqlUserHWIDsTable);
			sql(sqlBannedHWIDsTable);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public String getType() {
		return "MySQL";
	}
}
