package ru.NTSystem.NTConnector.Databases;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import ru.NTSystem.NTConnector.NTConnectorPlugin;

public class SQLiteStorage extends SQLStorage {
	private final String sqlUserTable =        "CREATE TABLE IF NOT EXISTS " + SQLStorage.usersTable + 
			                                  " (id INTEGER PRIMARY KEY, login TEXT UNIQUE NOT NULL, password TEXT NOT NULL, mail TEXT UNIQUE NOT NULL);";
	private final String sqlBannedHWIDsTable = "CREATE TABLE IF NOT EXISTS " + SQLStorage.banHWIDsTable +
			                                  " (id INTEGER PRIMARY KEY, hwid TEXT NOT NULL);";
	private final String sqlUserHWIDsTable =   "CREATE TABLE IF NOT EXISTS " + SQLStorage.usersHWIDsTable +
			                                  " (id INTEGER PRIMARY KEY, login TEXT NOT NULL, hwid TEXT NOT NULL);";

	private Connection conn;

	public SQLiteStorage(File dbf) throws ClassNotFoundException, SQLException {
			try {
				dbf.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
			Class.forName("org.sqlite.JDBC");
			conn = DriverManager.getConnection("jdbc:sqlite:"
					+ dbf.getAbsolutePath());
			createTables();
	}

	@Override
	public void registerUser(String name, String password, String email) {
		try {
			conn.createStatement().execute(String.format("INSERT INTO `%s` (`login`, `password`, `mail`) VALUES ('%s', '%s', '%s')",
					usersTable, name, password, email));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean authorizeUser(String name, String password) {
		String query = String.format("SELECT COUNT(*) FROM `%s` WHERE `login`='%s' AND `password`='%s'", usersTable, name, password);
		try {
			ResultSet rs = getSQL(query);
			rs.next();
			if (rs.getInt(1) != 0) {
				return true;
			}
			return false;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public void banHWID(String hwid) {
		sql(String.format("INSERT INTO `%s` (`hwid`) VALUES ('%s')", banHWIDsTable, hwid));
	}

	@Override
	public void banUser(String name) {
		for (String hwid : getUserHWIDs(name)) {
			banHWID(hwid);
		}
	}

	@Override
	public void addUserHWID(String user, String hwid) {
		sql(String.format("INSERT INTO `%s` (`login`, `hwid`) VALUES ('%s', '%s')", usersHWIDsTable, user, hwid));
	}

	@Override
	public List<String> getUserHWIDs(String name) {
		List<String> hwids = new ArrayList<String>();
		String query = String.format("SELECT `hwid` FROM `%s` WHERE `login`='%s'", usersHWIDsTable, name);
		try {
			ResultSet rs = getSQL(query);
			while (rs.next()) {
				hwids.add(rs.getString("hwid"));
			}
			return hwids;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return hwids;
	}

	@Override
	public boolean isRegistered(String name, String email) {
		String query = String.format("SELECT COUNT(*) FROM `%s` WHERE `login`='%s' AND `mail`='%s'", usersTable, name, email);
		try {
			PreparedStatement ps = conn.prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				if (rs.getInt(1) != 0) {
					ps.close();
					return true;
				}
			}
			ps.close();
			return false;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean isHWIDBanned(String hwid) {
		String query = String.format("SELECT COUNT(*) FROM `%s` WHERE `hwid`='%s'", banHWIDsTable, hwid);
		try {
			PreparedStatement ps = conn.prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				if (rs.getInt(1) != 0) {
					ps.close();
					return true;
				}
			}
			ps.close();
			return false;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public void sql(String sql) throws IllegalStateException {
		try {
			conn.createStatement().execute(sql);
		} catch (SQLException e) {
			NTConnectorPlugin.getInstance().logInfo("SQL error: " + e.getLocalizedMessage() +
					(e.getCause() != null ? " (" + e.getCause().getLocalizedMessage() + ")" : "") , 0);
		}
	}
	
	public ResultSet getSQL(String sql) throws IllegalStateException {
		try {
			return conn.prepareStatement(sql).executeQuery();
		} catch (SQLException e) {
			NTConnectorPlugin.getInstance().logInfo("SQL error: " + e.getLocalizedMessage() +
					(e.getCause() != null ? " (" + e.getCause().getLocalizedMessage() + ")" : "") , 0);
		}
		return null;
	}
	
	public void close() throws SQLException{
		conn.close();
	}

	public void createTables() {
		sql(sqlUserTable);
		sql(sqlUserHWIDsTable);
		sql(sqlBannedHWIDsTable);
	}

	@Override
	public String getType() {
		return "SQLite";
	}
}
