package ru.NTSystem.NTConnector.Databases;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import ru.NTSystem.NTConnector.InputDataHandler;

public abstract class Storage {
	public static Storage currentManager = null;

	public abstract void registerUser(String name, String password, String email);

	public abstract boolean authorizeUser(String name, String password);

	public abstract void banHWID(String hwid);

	public abstract void banUser(String name);

	public abstract void addUserHWID(String user, String hwid);

	public abstract List<String> getUserHWIDs(String name);

	public abstract boolean isRegistered(String name, String email);

	public abstract boolean isHWIDBanned(String hwid);

	public abstract String getType();

	public boolean hasUserHWID(String name, String hwid) {
		return getUserHWIDs(name).contains(hwid);
	}
	
	public void fromUsersXML(File xmlfile){
		BufferedReader br;
		String line, login, pass, mail;
		try {
			br = new BufferedReader(new FileReader(xmlfile));
			while ((line = br.readLine()) != null) {
				login = InputDataHandler.getXMLData(line, "login");
				pass = InputDataHandler.getXMLData(line, "password");
				mail = InputDataHandler.getXMLData(line, "mail");
				if(login == null || pass == null || mail == null) continue;
				registerUser(login, pass, mail);
			}
			br.close();
		} catch (FileNotFoundException e) {
			// slient
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Storage getManager(String type, String user, String pass,
			String host, int port, String dbase, File file,
			String customsqlreg, String customsqlauth, String customuserssql, String customisregsql,
			String customhwidstable, String customhwidsbanstable) throws ClassNotFoundException, SQLException {
		switch (type) {
		case "sqlite":
			currentManager = new SQLiteStorage(file);
			break;
		case "mysql":
			currentManager = new MySQLStorage(host, port, user, pass, dbase);
			break;
		case "dle":
			currentManager = new DLEStrorage(host, port, user, pass, dbase);
			break;
		case "custom":
			currentManager = new CustomStorage(customsqlreg, customsqlauth, customuserssql, customhwidsbanstable, customhwidstable, customisregsql, host, port, user, pass, dbase);
			break;
		default:
			currentManager = new YMLStorage(file);
			break;
		}
		return currentManager;
	}

	public static Storage getCurrentManager() {
		return currentManager;
	}
}
