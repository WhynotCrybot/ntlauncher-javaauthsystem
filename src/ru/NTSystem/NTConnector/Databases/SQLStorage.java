package ru.NTSystem.NTConnector.Databases;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public abstract class SQLStorage extends Storage {
	public static final String usersTable = "users";
	public static final String banHWIDsTable = "banned_hwids";
	public static final String usersHWIDsTable = "users_hwids";

	@Override
	public String getType() {
		return null;
	}

}
