package ru.NTSystem.NTConnector.Databases;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ru.NTSystem.NTConnector.NTConnectorPlugin;

public class CustomStorage extends SQLRemoteStorage {
	
	private String sqlreg, sqlauth, sqlisreg;
	
	public CustomStorage(String sqlreg, String sqlauth, String userssql,
			String bannedHWIDsTable, String usersHWIDsTable, String sqlisreg,
			String host, int port, String user, String pass,
			String db) throws ClassNotFoundException, SQLException {
		super(host, port, user, pass, db);
		this.sqlreg = sqlreg;
		this.sqlauth = sqlauth;
		this.sqlisreg = sqlisreg;
		sql(userssql);
		sql(MySQLStorage.sqlUserHWIDsTable.replace(SQLStorage.usersHWIDsTable, usersHWIDsTable));
		sql(MySQLStorage.sqlBannedHWIDsTable.replace(SQLStorage.banHWIDsTable, bannedHWIDsTable));
	}
	
	@Override
	public void registerUser(String name, String password, String email) {
		sql(sqlreg.replace("!login", name).replace("!password", password).replace("!email", email));
	}

	@Override
	public boolean authorizeUser(String name, String password) {
		String query = sqlauth.replace("!login", name).replace("!password", password);
		try {
			Connection conn = DriverManager.getConnection("jdbc:mysql://"
					+ host + ":" + port + "/" + db + "?autoReconnect=true&user=" + user
					+ "&password=" + pass);
			ResultSet rs = conn.createStatement().executeQuery(query);
			if(!rs.next()) return false; 
			if (rs.getInt(1) != 0) {
				rs.close();
				conn.close();
				return true;
			}
			rs.close();
			conn.close();
			return false;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public void banHWID(String hwid) {
		sql(String.format("INSERT INTO `%s` (`hwid`) VALUES ('%s')", banHWIDsTable, hwid));
	}

	@Override
	public void banUser(String name) {
		for (String hwid : getUserHWIDs(name)) {
			banHWID(hwid);
		}
	}

	@Override
	public void addUserHWID(String user, String hwid) {
		sql(String.format("INSERT INTO `%s` (`login`, `hwid`) VALUES ('%s', '%s')", usersHWIDsTable, user, hwid));
	}

	@Override
	public List<String> getUserHWIDs(String name) {
		List<String> hwids = new ArrayList<String>();
		String query = String.format("SELECT `hwid` FROM `%s` WHERE `login`='%s'", usersHWIDsTable, name);
		try {
			Connection conn = DriverManager.getConnection("jdbc:mysql://"
					+ host + ":" + port + "/" + db + "?autoReconnect=true&user=" + user
					+ "&password=" + pass);
			ResultSet rs = conn.createStatement().executeQuery(query);
			while (rs.next()) {
				hwids.add(rs.getString("hwid"));
			}
			rs.close();
			conn.close();
			return hwids;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return hwids;
	}

	@Override
	public boolean isRegistered(String name, String email) {
		String query = String.format(sqlisreg, name, email);
		try {
			Connection conn = DriverManager.getConnection("jdbc:mysql://"
					+ host + ":" + port + "/" + db + "?autoReconnect=true&user=" + user
					+ "&password=" + pass);
			ResultSet rs = conn.createStatement().executeQuery(query);
			while (rs.next()) {
				if (rs.getInt(1) != 0) {
					rs.close();
					conn.close();
					return true;
				}
			}
			rs.close();
			conn.close();
			return false;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean isHWIDBanned(String hwid) {
		String query = String.format("SELECT COUNT(*) FROM `%s` WHERE `hwid`='%s'", banHWIDsTable, hwid);
		try {
			Connection conn = DriverManager.getConnection("jdbc:mysql://"
					+ host + ":" + port + "/" + db + "?autoReconnect=true&user=" + user
					+ "&password=" + pass);
			ResultSet rs = conn.createStatement().executeQuery(query);
			while (rs.next()) {
				if (rs.getInt(1) != 0) {
					rs.close();
					conn.close();
					return true;
				}
			}
			rs.close();
			conn.close();
			return false;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public String getType() {
		return "custom";
	}

}