package ru.NTSystem.NTConnector;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.plugin.java.JavaPlugin;

import ru.NTSystem.NTConnector.Databases.Storage;

public class NTConnectorPlugin extends JavaPlugin implements Listener {

	public ServerSocket ss;
	public boolean stopping = false;
	public ConfigurationSection settings;
	public InputDataHandler ih;
	public SocketThread mainThread;
	
	public static NTConnectorPlugin inst;

	@Override
	public void onLoad() {
		saveDefaultConfig();
		loadSettings();
	}

	@Override
	public void onEnable() {
		inst = this;
		Bukkit.getPluginManager().registerEvents(this, this);
		try {
			reconnect();
		} catch (Throwable e) {
			getLogger().warning(
					"Error opening storage "
							+ settings.getString("db.type", "file") + ": "
							+ e.getMessage() + (e.getCause() != null ? ": " + e.getCause().getClass().getName() + ": " + e.getCause().getMessage() : ""));
			Bukkit.getPluginManager().disablePlugin(this);
			return;
		}
		openSocket();
	}
	
	public void reconnect() throws ClassNotFoundException, SQLException{
		ih = new InputDataHandler(this);
		Storage db = Storage.getManager(
				settings.getString("db.type", "file"),
				settings.getString("db.user", "root"),
				settings.getString("db.pass", "root"),
				settings.getString("db.host", "localhost"),
				settings.getInt("db.port", 3306),
				settings.getString("db.dbname", "dbname"),
				new File(getDataFolder(), settings.getString("db.filename",
						"storage.yml")),
				settings.getString("db.customsqlreg", "INSERT INTO `tablename` (`login`, `password`, `mail`) VALUES ('!login', '!password', '!email')"),
				settings.getString("db.customsqlauth", "SELECT COUNT(*) FROM `tablename` WHERE `login`='!login' AND `password`='!password'"),
				settings.getString("db.customuserssql", "CREATE TABLE IF NOT EXISTS `ms_members` (`id` INT(5) NOT NULL AUTO_INCREMENT,`usr` varchar(150) NOT NULL,"
						+ "`pass` varchar(96) NOT NULL,`email` varchar(150) NOT NULL, UNIQUE KEY `usr` (`usr`,`email`), PRIMARY KEY (`id`))"),
				settings.getString("db.customsqlisreg", "SELECT COUNT(*) FROM `ms_members` WHERE `usr`='!login' AND `email`='!email'"),
				settings.getString("db.customhwidstable", "users_hwids"),
				settings.getString("db.customhwidsbanstable", "banned_hwids"));
		logInfo("Using " + db.getType() + " storage type", 1);
	}

	private void openSocket() {
		try {
			ss = new ServerSocket(settings.getInt("port", 65533));
			mainThread = new SocketThread(this);
			mainThread.start();
			getLogger().info(
					"Started server manager at port "
							+ settings.getInt("port", 65533));
		} catch (IOException e1) {
			e1.printStackTrace();
			Bukkit.getPluginManager().disablePlugin(this);
			return;
		}
	}

	private void loadSettings() {
		settings = getConfig().getRoot();
	}

	@Override
	public void onDisable() {
		getServer().getScheduler().cancelTasks(this);
		closeSocket();
	}

	private void closeSocket() {
		try {
			stopping = true;
			if (mainThread != null)
				mainThread.interrupt();
			if (ss != null)
				ss.close();
			getLogger().info("Socket closed");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@EventHandler
	public void onLogin(PlayerLoginEvent e) {
		if (!ih.accessList.containsKey(e.getPlayer().getName())) {
			logInfo(e.getPlayer().getName()
					+ " was kicked because he is not autorized", 1);
			e.disallow(Result.KICK_WHITELIST, "You are not authorized!");
		} else if(settings.getBoolean("removeaccess", true))
			ih.accessList.remove(e.getPlayer().getName());
	}

	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {
		try {
			if(command.getName().equalsIgnoreCase("ntreload")){
				if (sender.isOp()){
					reloadConfig();
					sender.sendMessage("Reloaded");
				} else {
					sender.sendMessage("You dont have permissions");
				}
				return true;
			}
			if (args.length >= 1) {
				if (sender.isOp()) {
					if (command.getName().equalsIgnoreCase("banuser"))
						Storage.getCurrentManager().banUser(args[0]);
					else if (command.getName().equalsIgnoreCase("banhwid"))
						Storage.getCurrentManager().banHWID(args[0]);
					sender.sendMessage("Banned");
				} else
					sender.sendMessage("You dont have permissions");
				return true;
			}
		} catch (Throwable e) {
		}
		return false;
	}

	public void logInfo(String mes, int level) {
		if (settings.getInt("log-level") >= level)
			getLogger().info(mes);
	}
	
	public static NTConnectorPlugin getInstance(){
		return inst;
	}

}
